from flask import Flask
try:    
    from sudoku import SudokuTable
except:
    pass
from flask import request
from sudoku import SudokuTable

app = Flask(__name__)

@app.route("/postjson", methods = ['POST'])
def process_sudoku_solver_request():
    content = request.get_json()
    for k, v in content.items():
        initial_conditions = [ line.split(',') for line in v.split(';')]
        for i in range(len(initial_conditions)):
            for j in range(len(initial_conditions)):
                initial_conditions[i][j]=int(initial_conditions[i][j])

    sudoku_table = SudokuTable(initial_conditions)
    result = sudoku_table.solve_the_sudoku_game2()
    return result

def run_app_for_graphical_interface():
    app.run(host='127.0.0.1')

if __name__ == "__main__":
    run_app_for_graphical_interface()
