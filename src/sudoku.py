
from cell import *
import cell

class SudokuTable():
    def __init__(self, initial_conditions):
        self.initialize_the_sudoku_game(initial_conditions)

    def initialize_the_sudoku_game(self, initial_conditions):
        self.create_the_sudoku_table()
        self.set_the_initial_conditions(initial_conditions)
        self.connect_the_cells()

    def create_the_sudoku_table(self):
        self.game_table = []

        for i in range(Cell.MAX_I+1):
            line = ""
            row = []
            for j in range(Cell.MAX_J+1):
                cell_ij = Cell(i, j, self)
                row.append(cell_ij)
                line = line + "(" + str(cell_ij.i) + " "+ str(cell_ij.j) + ")"
            self.game_table.append(row)
        Cell.game_table = self.game_table

    def get_previous_cell_by_ij(self, i, j):
        if i == Cell.MIN_I and j == Cell.MIN_J:
            return None
        elif i == Cell.MIN_I and j != Cell.MIN_J:
            return self.get_cell_ij(Cell.MAX_I, j-1)

        return self.get_cell_ij(i-1, j)

    def get_next_cell_by_ij(self, i, j):
        if i == Cell.MAX_I and j == Cell.MAX_J:
            return None
        if i == Cell.MAX_I:
            return self.get_cell_ij(Cell.MIN_I, j+1)

        return self.get_cell_ij(i+1, j)

    def connect_the_cells(self):
        for i in range(len(self.game_table)):
            for j in range(len(self.game_table[i])):
                cell_ij = self.get_cell_ij(i, j)
                previous_cell = self.get_previous_cell_by_ij(i, j)
                next_cell = self.get_next_cell_by_ij(i, j)
                cell_ij.set_next_cell(next_cell)
                cell_ij.set_previous_cell(previous_cell)

    def set_the_initial_conditions(self, initial_conditions):
        for j in range(len(initial_conditions)):
            line = ""
            condition_row_j = initial_conditions[j]
            for i in range(len(condition_row_j)):
                condition_ij = condition_row_j[i]
                cell_ij = self.get_cell_ij(i, j)
                cell_ij.set_initial_conditions(condition_ij)
                line = line + "(" + str(condition_ij) +")"

    def get_cell_ij(self, i, j):
        column_i = self.game_table[i]
        cell_ij = column_i[j]
        return cell_ij

    def get_game_table_row_j(self, j):
        game_table_row_j = []
        for i in range(len(self.game_table)):
            game_table_row_j.append(self.get_cell_ij(i, j))
        return game_table_row_j

    def get_game_table_column_i(self, i):
        return self.game_table[i]

    def convert_cell_list_to_value_list(cell_list):
        list_of_values = []
        for cell in cell_list:
            list_of_values.append(cell.value)
        return list_of_values

    def convert_game_table_row_j_to_list_of_values(self, j):
        row_j = self.get_game_table_row_j(j)
        return SudokuTable.convert_cell_list_to_value_list(row_j)

    def convert_game_table_column_i_to_list_of_values(self, i):
        column_i = self.get_game_table_column_i(i)
        return SudokuTable.convert_cell_list_to_value_list(column_i)

    def is_this_value_feasible_for_row_j(self, value, j):
        list_of_values_in_row_j = self.convert_game_table_row_j_to_list_of_values(j)
        is_value_feasible_in_row_j = not (value in list_of_values_in_row_j)
        return is_value_feasible_in_row_j

    def is_this_value_feasible_for_column_i(self, value, i):
        list_of_values_in_column_i = self.convert_game_table_column_i_to_list_of_values(i)
        is_value_feasible_in_column_i = not (value in list_of_values_in_column_i)
        return is_value_feasible_in_column_i

    def get_values_of_square_where_ij_belong(self, i, j):
        cutoff_first_column_or_row = 3
        cutoff_second_column_or_row = 6

        if i < cutoff_first_column_or_row and j < cutoff_first_column_or_row:
            condition_i = lambda xi: xi < cutoff_first_column_or_row
            condition_j = lambda xj: xj < cutoff_first_column_or_row
        elif i < cutoff_second_column_or_row and j < cutoff_first_column_or_row:
            condition_i = lambda xi: xi < cutoff_second_column_or_row and xi > (cutoff_first_column_or_row-1)
            condition_j = lambda xj: xj < cutoff_first_column_or_row
        elif j < cutoff_first_column_or_row:
            condition_i = lambda xi: xi > (cutoff_second_column_or_row-1)
            condition_j = lambda xj: xj < cutoff_first_column_or_row
        elif i < cutoff_first_column_or_row and j < cutoff_second_column_or_row:
            condition_i = lambda xi: xi < cutoff_first_column_or_row
            condition_j = lambda xj: xj < cutoff_second_column_or_row and xj > (cutoff_first_column_or_row-1)
        elif i < cutoff_second_column_or_row and j < cutoff_second_column_or_row:
            condition_i = lambda xi: xi < cutoff_second_column_or_row and xi > (cutoff_first_column_or_row-1)
            condition_j = lambda xj: xj < cutoff_second_column_or_row and xj > (cutoff_first_column_or_row-1)
        elif j < cutoff_second_column_or_row:
            condition_i = lambda xi: xi > (cutoff_second_column_or_row-1)
            condition_j = lambda xj: xj < cutoff_second_column_or_row and xj > (cutoff_first_column_or_row-1)
        elif i < cutoff_first_column_or_row:
            condition_i = lambda xi: xi < cutoff_first_column_or_row
            condition_j = lambda xj: xj > (cutoff_second_column_or_row-1)
        elif i < cutoff_second_column_or_row:
            condition_i = lambda xi: xi < cutoff_second_column_or_row and xi > (cutoff_first_column_or_row-1)
            condition_j = lambda xj: xj > (cutoff_second_column_or_row-1)
        else:
            condition_i = lambda xi: xi > (cutoff_second_column_or_row-1)
            condition_j = lambda xj: xj > (cutoff_second_column_or_row-1)

        square_ij_values = []
        for i in range(len(self.game_table)):
            for j in range(len(self.game_table)):
                if condition_i(i) and condition_j(j):
                    cell_ij = self.get_cell_ij(i, j)
                    value_ij = cell_ij.value
                    square_ij_values.append(value_ij)

        return square_ij_values

    def is_this_value_feasible_for_square_ij(self, value, i, j):
        values_square_where_ij_belong = self.get_values_of_square_where_ij_belong(i, j)
        is_value_in_the_square = not (value in values_square_where_ij_belong)
        return is_value_in_the_square

    def print_the_game_table(self):
        for j in range(len(self.game_table)):
            line = ""
            for i in range(len(self.game_table)):
                cell_ij = self.get_cell_ij(i, j)
                line = line + " " + str(cell_ij.value)
            print(line)

    def get_result_of_the_game_table(self):
        result = ""
        for j in range(len(self.game_table)):
            for i in range(len(self.game_table)):
                cell_ij = self.get_cell_ij(i, j)
                result = result + " " + str(cell_ij.value)
            result += "\n"
        return result

    def solve_the_sudoku_game(self):
        cell_00 = self.get_cell_ij(0, 0)
        cell_00.solve()
        self.print_the_game_table()

    def solve_the_sudoku_game2(self):
        cell_00 = self.get_cell_ij(0, 0)
        cell_00.solve()
        return self.get_result_of_the_game_table()
