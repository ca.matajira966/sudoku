# Python Sudoku Solver with Tkinter 

![alt text](.img/sudoku1.png "Fill your sudoku")
![alt text](.img/sudoku2.png "Click solve!")

## Objective

Practice:
1. Test Driven Development in Python (see tests.py).
2. Create a simple API in flask for the solver algorithm (solver api).
3. Create a simple Tkinter GUI.
3. Write clean code using Bob Martin's philosophy: self-documentary code, no commenting, etc.

## Prerequisites
Python 3.82
Flask  1.1.2 (not necesarily the latest)

What things you need to install the software and how to install them

```
python3 -m virtualenv my_env
source my_env/bin/activate
pip3 install flask==1.1.2
```
Or
```
sudo apt-get update && sudo apt-get install python3-flask
```

## Running
```
python3 ./src/guit_tkinter.py

```
## Running Unit Tests
```
python3 -m unittest -v tests.py
```

## Author

* Camilo MATAJIRA see www.camilomatajira.com
